# 3 steps to defensive programming to resist bugs
# - write docstrings for functions
# - build modular programs
# - check conditions of io
# 
# document assumptions in code designing
# dont have "motherhood and apple pie approach" 
# question what you are doing because others might not approach it the same way
# 
# ready to test when code runs and you have a set of expected results
# 
# unit testing:
# validating each function separately
# 
# regresion testing:
# run the same test on the same module to catch any reintroduced errors
# 
# integration testing:
# test the program as a whole
# test how modules interact with each other
# 
# black box testing:
# explores path through specification 
# designed without looking at code
# done by someone other than implementor
# reusable testing
# natural boundaries to functions (small numbers vs large numbers, empty lists vs list of one vs list of many)  
# 
# glass box testing:
# explores path through code
# uses code to design tests
# path complete, goes through each possible branch
# excercises all parts of loops
# 
# what to do when you get bugs:
# isolate bug
# eradicate bug
# retest
# 
# covert vs overt bugs
# overt has obvious failure, crashing, or infinite loop
# covert returns a false value
# 
# persistent vs intermittent bugs:
# persistent occurs every time code is run
# intermittent occurs sometimes, even on same input
# 
# want to steer bugs toward the overt and persistent category
# overt and intermittent are harder to debug but conditions need to be found to prompt the bug
# 
# covert bugs are dangerous because it takes a long time to realize that it was giving wrong data
# 
# steep learning curve
# be systematic in searches
# use print statements
# 
# easy error messages
# indexerror, typeerror, nameerror, syntaxerror
# 
# logic errors 
# think before writing code
# draw pictures 
# explain the code to someone else
# 
#   