# exceptions raised when conditions are not what are expected
# 
# fail silently: substitute default values or continue
# return error value: needs to check for special value
# stop execution: raises an exception with a message
# 
# try/except blocks are used to fail silently but redirect execution towards a good block of code
# 
# can use else or finally to run after try/except clauses
# 
# raise keyword can be used to raise exceptions when incorrect data is parsed
#  
